import GUI
var logo : int := Pic.FileNew ("images/logo.bmp")
Pic.Draw (logo, -113, -350, picCopy)

Draw.FillBox (0, 350, 700, 400, black)
Draw.FillBox (0, 50, 700, 0, black)

var ohGAWD : int := Font.New ("Euclid Fraktur:34")
var ohGAWD2 : int := Font.New ("Hobo Std:15")

Font.Draw ("Balvidor", 230, 360, ohGAWD, white)
Font.Draw ("Graham Pegg", 10, 10, ohGAWD2, white)
Font.Draw ("Journey Cuave", 500, 10, ohGAWD2, white)

var thing : string
var signText : string
var dead : boolean := false
var direction : string
var tempInt : int
var tempString : string := ""
var tempArray : array 1 .. 256 of string
var tempArray2 : array 1 .. 256 of string
var streamNo : int
var fileOutput : string
var gtfomess : string
var signsText : string
var mess : array 1 .. 256 of string
var signs : array 1 .. 256 of string
var blocks : array 1 .. 256 of string
var playerName : string
var playerMess : string
var tempBlocks : string
var tempMess : string
var tempSigns : string
var oldBlock : int := 0
var x : int

var i : int
var j : int := 1

function explodeStringCount (explode : string, explodeBy : int) : int

    var newExplode := explode + chr (explodeBy)

    var count := 0

    for i : 1 .. length (newExplode)

	if newExplode (i) = chr (explodeBy) then

	    count := count + 1

	end if

    end for

    result count

end explodeStringCount

function explodeString (explode : string, explodeBy : int) : array 1 .. 256 of string

    var defaultArray : array 1 .. 256 of string

    var tempString : string := ""

    var newExplode := explode + chr (explodeBy)

    var count := 0

    for i : 1 .. length (newExplode)

	if not (newExplode (i) = chr (explodeBy)) then

	    tempString := tempString + newExplode (i)

	else

	    count := count + 1

	    defaultArray (count) := tempString

	    tempString := ""

	end if

    end for

    result defaultArray

end explodeString

proc die
    put "The End"
    for q : 1 .. (x - 1)
	tempArray (q) := blocks (q)
	tempArray2 (q) := mess (q)
    end for
    tempArray (x) := "NOPLAYER"
    tempArray2 (x) := "NOMESS"
    tempArray (oldBlock) := "NOPLAYER"
    tempArray2 (oldBlock) := "NOMESS"
    for q : (x + 1) .. 9
	tempArray (q) := blocks (q)
	tempArray2 (q) := mess (q)
    end for
    tempString := ""
    tempBlocks := ""
    tempMess := ""
    tempSigns := ""
    for w : 1 .. 9
	tempBlocks := tempBlocks + tempArray (w) + "_"
	tempMess := tempMess + tempArray2 (w) + "_"
	tempSigns := tempSigns + signs (w) + "_"
    end for
    tempString := tempBlocks + "\n"
    tempString := tempString + tempMess + "\n"
    streamNo := 0
    File.Delete ("players.txt")
    open : streamNo, "players.txt", put
    put : streamNo, tempString
    put : streamNo, (tempSigns + "\n")
    close : streamNo

    dead := true
end die

proc doathing
    put "What would you like to do?> " ..
    get thing

    if (thing = "sign") then
	put "What Do You Want To Write On the Sign?> " ..
	get signText
	put "You plant a sign in the groud and chuck the old one into the pile of old signs to your left."
	for q : 1 .. (x - 1)
	    tempArray (q) := signs (q)
	end for
	tempArray (x) := signText
	for q : (x + 1) .. 9
	    tempArray (q) := signs (q)
	end for
	tempString := ""
	tempBlocks := ""
	tempMess := ""
	tempSigns := ""
	for w : 1 .. 9
	    tempBlocks := tempBlocks + blocks (w) + "_"
	    tempMess := tempMess + mess (w) + "_"
	    tempSigns := tempSigns + tempArray (w) + "_"
	end for
	tempString := tempBlocks + "\n"
	tempString := tempString + tempMess + "\n"
	streamNo := 0
	File.Delete ("players.txt")
	open : streamNo, "players.txt", put
	put : streamNo, tempString
	put : streamNo, (tempSigns + "\n")
	close : streamNo
	delay (4000)
	cls
    elsif (thing = "leave") then
	put "North, East, South, or West?> " ..
	get direction
	if (direction = "west") then
	    oldBlock := x
	    if (i = 1) then
		i := 4
	    end if
	    if not (j = 1) then
		if (j = 2) then
		    x := i + 3
		end if
		if (j = 3) then
		    x := i + 6
		end if
	    else
		x := i
	    end if
	    i := i - 1
	elsif (direction = "east") then
	    oldBlock := x
	    if (i = 3) then
		i := 0
	    end if
	    if not (j = 1) then
		if (j = 2) then
		    x := i + 3
		end if
		if (j = 3) then
		    x := i + 6
		end if
	    else
		x := i
	    end if
	    if (i = 3) then
		i := 0
	    end if
	    i := i + 1
	elsif (direction = "north") then
	    oldBlock := x
	    if (j = 1) then
		j := 4
	    end if
	    if not (j = 1) then
		if (j = 2) then
		    x := i + 3
		end if
		if (j = 3) then
		    x := i + 6
		end if
	    else
		x := i
	    end if
	    if (j = 1) then
		j := 4
	    end if
	    j := j - 1
	elsif (direction = "south") then
	    oldBlock := x
	    if (j = 3) then
		j := 0
	    end if
	    if not (j = 1) then
		if (j = 2) then
		    x := i + 3
		end if
		if (j = 3) then
		    x := i + 6
		end if
	    else
		x := i
	    end if
	    if (j = 3) then
		j := 0
	    end if
	    j := j + 1
	end if
    elsif (thing = "quit") then
	blocks := explodeString (fileOutput, 95)
	blocks (oldBlock) := "NOPLAYER"
	mess := explodeString (gtfomess, 95)
	signs := explodeString (signsText, 95)
	for q : 1 .. (x - 1)
	    tempArray (q) := blocks (q)
	end for
	tempArray (x) := "NOPLAYER"
	for q : (x + 1) .. 9
	    tempArray (q) := blocks (q)
	end for
	tempString := ""
	tempBlocks := ""
	tempMess := ""
	tempSigns := ""
	for w : 1 .. 9
	    tempBlocks := tempBlocks + tempArray (w) + "_"
	    tempMess := tempMess + mess (w) + "_"
	    tempSigns := tempSigns + signs (w) + "_"
	end for
	tempString := tempBlocks + "\n"
	tempString := tempString + tempMess + "\n"
	streamNo := 0
	File.Delete ("players.txt")
	open : streamNo, "players.txt", put
	put : streamNo, tempString
	put : streamNo, (tempSigns + "\n")
	close : streamNo

	die
    end if
end doathing

proc makegametime
    Music.PlayFileLoop ("sound/Main+loop.wav")

    cls
    colorback (red)
    cls
    color (white)

    put "What is your screen name?> " ..
    get playerName

    put "What is your personal message?> " ..
    get playerMess


    put "Enter Your Starting Block!> " ..
    get tempInt

    loop
	exit when tempInt <= 3
	tempInt := tempInt - 3
	j := j + 1
    end loop
    i := tempInt

    loop
	open : streamNo, "players.txt", get, mod
	get : streamNo, fileOutput
	get : streamNo, gtfomess
	get : streamNo, signsText
	close : streamNo

	blocks := explodeString (fileOutput, 95)
	mess := explodeString (gtfomess, 95)
	signs := explodeString (signsText, 95)
	if not (j = 1) then
	    if (j = 2) then
		x := i + 3
	    end if
	    if (j = 3) then
		x := i + 6
	    end if
	else
	    x := i
	end if
	if (x = 0) then
	    x := 9
	    i := 3
	    j := 1
	end if
	put "Your in block ", x
	if (blocks (x) = "NOPLAYER") then
	    put "There is no one here"
	    for q : 1 .. (x - 1)
		if (q = oldBlock) then
		    tempArray (oldBlock) := "NOPLAYER"
		    tempArray2 (oldBlock) := "NOMESS"
		else
		    tempArray (q) := blocks (q)
		    tempArray2 (q) := mess (q)
		end if
	    end for
	    tempArray (x) := playerName
	    tempArray2 (x) := playerMess
	    for q : (x + 1) .. 9
		if (q = oldBlock) then
		    tempArray (oldBlock) := "NOPLAYER"
		    tempArray2 (oldBlock) := "NOMESS"
		else
		    tempArray (q) := blocks (q)
		    tempArray2 (q) := mess (q)
		end if
	    end for
	    if (oldBlock not= 0) then
		tempArray (oldBlock) := "NOPLAYER"
		tempArray2 (oldBlock) := "NOMESS"
	    end if
	    tempString := ""
	    tempBlocks := ""
	    tempMess := ""
	    tempSigns := ""
	    for w : 1 .. 9
		tempBlocks := tempBlocks + tempArray (w) + "_"
		tempMess := tempMess + tempArray2 (w) + "_"
		tempSigns := tempSigns + signs (w) + "_"
	    end for
	    tempString := tempBlocks + "\n"
	    tempString := tempString + tempMess + "\n"
	    streamNo := 0
	    File.Delete ("players.txt")
	    open : streamNo, "players.txt", put
	    put : streamNo, tempString
	    put : streamNo, (tempSigns + "\n")
	    close : streamNo

	    put "A sign says \"", signs (x), "\""
	    doathing
	else
	    put blocks (x), " is here! :3"
	    put blocks (x), " says \"", mess (x), "\""
	    if (blocks (x) = "Menataur" or blocks (x) = "cat") then
		blocks (x) := "Menataur"
		put "You Died Of Dysentary!!!"
		die
	    else
		if (x > 2) then
		    put "You ran to block ", (x - 1)
		    i := i - 1
		else
		    put "You ran to block 9"
		    i := 9
		end if
	    end if
	end if
	exit when dead = true
    end loop

end makegametime

proc helpmeh
    cls
    colorback (red)
    cls
    color (white)

    put "Welcome! :3"
    put "Balvidor is a huge game of cat and mouse set in a medival setting! :D"
    put "To start, click the start button, one user should enter the name 'cat' to act as the cat in the game."
    put "All other players try to avoid the cat."
    put "Any user can enter 3 commands in-game:"
    put "       - leave"
    put "           *move to another block of the game"
    put "       - sign"
    put "           *place a sign down"
    put "       - quit"
    put "           *end game"
    put "Enjoy! :3"
    put "<Something>+<Enter> to contine ;3"
    get tempString
    makegametime
end helpmeh

var startBtn : int := GUI.CreateButton (250, 180, 0, "Start", makegametime)
var helpBtn : int := GUI.CreateButton (330, 180, 0, "Help!", helpmeh)
loop
    exit when GUI.ProcessEvent
end loop
